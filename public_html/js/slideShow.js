var capArray = [];
var imgArray = [];
var arrayIndex = 0;
var nextButton;
var playButton;
var prevButton;
var play = true;
var Image;
var Caption;

$(function() {
    
    $.getJSON('js/slideShow1.json', function(data){
          
          $.each(data, function(key, val){
                capArray.push(key);
                imgArray.push(val);
                
                
                console.log("Images: " + val + "\nCaption: " + key);
                });   
                setUpSlideShow();
                startSlideShow();
    });
    
    
});


function setUpSlideShow(){
    
    console.log(imgArray[0]);
    console.log(capArray[0]);
    
    var slideshow = document.getElementById("slideShow01");
    slideshow.innerHTML = "<div><img id=\"slideImg\" class=\"slideShowImage\"><p id=\"slideCaption\"></p><div class=\"icon\"><table align=\"center\"><tr><td><img id=\"prevButt\" onclick=\"lastImage()\"></td><td><img id=\"playButt\" onclick=\"startSlideShow()\"></td><td><img id=\"nextButt\"  onclick=\"nextImage()\"></td></tr></table></div></div>";
    
    
    Image = document.getElementById("slideImg");
    Caption = document.getElementById("slideCaption");
    Image.src = imgArray[0];
    Caption.innerHTML = capArray[0];
    
    playButton = document.getElementById("playButt");
    prevButton = document.getElementById("prevButt");
    nextButton = document.getElementById("nextButt");
    
    playButton.src = "img/slideShow/stop.png";
    prevButton.src = "img/slideShow/Previous.png";
    nextButton.src = "img/slideShow/Next.png";
    
}
function lastImage(){

    console.log("MOVE BACK");    
    if(arrayIndex == 0){
        Image.src=imgArray[imgArray.length-1];
        Caption.innerHTML = capArray[capArray.length-1];
        arrayIndex = imgArray.length-1;
    }else{
        Image.src=imgArray[arrayIndex-1];
        Caption.innerHTML = capArray[arrayIndex-1];
        arrayIndex = arrayIndex -1;
    }
}

function nextImage(){

    console.log("Move Foward");
    if(arrayIndex == (imgArray.length-1)){
        Image.src = imgArray[0];
        Caption.innerHTML = capArray[0];
        arrayIndex = 0;
    }else{
        Image.src = imgArray[arrayIndex+1];
        Caption.innerHTML = capArray[arrayIndex+1];
        arrayIndex = arrayIndex + 1
    }
}

var intervalID;

function startSlideShow(){
    play = !play;
    console.log(play);
    if(play == true){
        playButton.src="img/slideShow/stop.png";
        playSlideShow();
    }else{
        playButton.src="img/slideShow/play.png";
        clearInterval(intervalID);
    }

}

function playSlideShow(){

    intervalID = setInterval(function(){nextImage()}, 2000);
}

